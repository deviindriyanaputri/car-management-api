# Challenge Chapter 6 - Car Management API
Repository ini menggunakan service repository pattern.
Role jalannya aplikasi:
- Terdapat endpoint untuk login sebagai superadmin, admin, maupun member.
- Terdapat endpoint untuk menambahkan admin yang mana hanya boleh dilakukan oleh superadmin.
- Terdapat endpoint untuk registrasi sebagai member.
- Terdapat 4 endpoint untuk melakukan **CRUD** terhadap data mobil, dan hanya admin dan superadmin saja yang dapat melakukan operasi tersebut.
- Terdapat endpoint untuk melihat daftar mobil yang tersedia.
- Terdapat endpoint untuk melihat **current user** dari token yang dimiliki.
- Setiap data mobil mempunyai informasi berikut:
  - Siapa yang membuat data tersebut
  - Siapa yang menghapus data tersebut
  - Siapa yang terakhir kali mengupdate data tersebut
- Menggunakan Service Repository Pattern dalam membangun project ini.
- Terdapat halaman yang menampilkan dokumentasi API, baik itu menggunakan Swagger UI, Redoc atau Library lain di dalam HTTP Server tersebut.
- Terdapat endpoint yang merespon dengan Open API document dari REST API yang dibangun dalam bentuk JSON.

## Getting Started
Untuk menjalankan development server, tinggal jalanin salah satu script di package.json, yang namanya `develop` atau `start`.

```sh
npm install
npm run start
```

## Database Management

Di dalam repository ini sudah terdapat beberapa script yang dapat digunakan dalam memanage database, yaitu:

- `npm db:create` digunakan untuk membuat database
- `npm db:drop` digunakan untuk menghapus database
- `npm db:migrate` digunakan untuk menjalankan database migration
- `npm db:seed` digunakan untuk melakukan seeding
- `npm db:rollback` digunakan untuk membatalkan migrasi terakhir
