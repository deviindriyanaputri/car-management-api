'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

     await queryInterface.bulkInsert(
      "Users",
      [
        {
          username: "superadmin",
          email: "superadmin@gmail.com",
          password: "$2a$10$P2s85MXUk1URquV2rnbwkOhnYJw8PDu7ITqYhc/ppM1aLMFhqiFIS",
          role: "superadmin",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: "admin",
          email: "admin@gmail.com",
          password: "$2a$10$Yu.rQkMr579SZA6HJDadn.leE4.UBsyNRSG/Z3XhOgy.iQU1iMKJ2",
          role: "admin",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: "member",
          email: "member@gmail.com",
          password: "$2a$10$evJ4YtTSZugO4iMG2fhf9.9KyX0Fz1aDuqxdKR1dWPrpH4xn4ocCe",
          role: "member",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
