 const carService = require("../../../services/carService");

 module.exports = {
   list(req, res) {
     carService
       .list()
       .then(({ data, count }) => {
         res.status(200).json({
           status: "Berhasil ditampilkan",
           data: { cars: data },
           meta: { total: count },
         });
       })
       .catch((err) => {
         res.status(400).json({
           status: "Gagal ditampilkan",
           message: err.message,
         });
       });
   },
 
   create(req, res) {
     carService
       .create(req.body)
       .then((post) => {
         res.status(201).json({
           status: "Data car sukses dibuat",
           data: post,
           createdBy : req.user.name
         });
       })
       .catch((err) => {
         res.status(422).json({
           status: "Gagal",
           message: err.message,
         });
       });
   },

  async update(req, res) {
    try {
      const cars = await carService.update(req.params.id, req.body);
      res.status(200).json({
        status: "Update data sukses !!!",
        updatedBy: req.user.name
      })
    } catch (err) {
      res.status(400).json({
        status: "Gagal",
        errors: [err.message]
      })
    }
  },
 
   show(req, res) {
     carService
       .get(req.params.id)
       .then((post) => {
         res.status(200).json({
           status: "Car dengan id " + req.params.id,
           data: post
           
         });
       })
       .catch((err) => {
         res.status(422).json({
           status: "Gagal",
           message: err.message,
         });
       });
   },
 
   destroy(req, res) {
     carService
       .delete(req.params.id)
       .then(() => {
          res.status(200).json({
            status:"Delete Car Berhasil",
            deletedBy: req.user.name
       });
      })
       .catch((err) => {
         res.status(422).json({
           status: "Gagal",
           message: err.message,
         });
       });
   },
 };
 