const userService = require("../../../services/userService")
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const SALT = 10;

function encryptPassword(originPassword) {
  return new Promise((resolve, reject) => {
    bcrypt.hash(originPassword, SALT, (err, password) => {
      if (!!err) {
        reject(err);
        return;
      }

      resolve(password);
    });
  });
}

function checkPassword(password, originPassword) {
  return new Promise((resolve, reject) => {
    bcrypt.compare(originPassword, password, (err, isPasswordCorrect) => {
      if (!!err) {
        reject(err);
        return;
      }

      resolve(isPasswordCorrect);
    });
  });
}

function createToken(payload) {
  return jwt.sign(payload, process.env.JWT_SIGNATURE_KEY || "Token");
}

module.exports = {
  async register(req, res) {
    const name = req.body.name;
    const email = req.body.email;
    const password = await encryptPassword(req.body.password);
    const role = req.body.role;
    const user = await userService.create({ 
      name, 
      email, 
      password, 
      role 
    });
    res.status(201).json({
      id: user.id,
      name:user.name,
      email: user.email,
      password: user.password,
      role: user.role,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
    });
  },

  async login(req, res) {
    const email = req.body.email.toLowerCase();
    const password = req.body.password;

    const user = await userService.findOne({
      where: { email },
    });

    if (!user) {
      res.status(404).json({ message: "Email tidak ditemukann!" });
      return;
    }

    const isPasswordCorrect = await checkPassword(
      user.password,
      password
    );

    if (!isPasswordCorrect) {
      res.status(401).json({ message: "Password salah!" });
      return;
    }

    const token = createToken({
      id: user.id,
      name:user.name,
      email: user.email,
      role:user.role,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
    });

    res.status(201).json({
      id: user.id,
      name:user.name,
      email: user.email,
      role:user.role,
      token,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
    });
  },

  async whoAmI(req, res) {
    res.status(200).json(req.user);
  },

  async authorize(req, res, next) {
    try {
      const bearerToken = req.headers.authorization;
      const token = bearerToken.split("Bearer ")[1];
      console.log(token)

      // const token = bearerToken.split("Bearer ")[1];k
      const tokenPayload = jwt.verify(
        token,
        process.env.JWT_SIGNATURE_KEY || "Token"
      );

      req.user = await userService.findByPk(tokenPayload.id);
      next();
    } catch (err) {
      console.error(err);
      res.status(401).json({
        message: "Unauthorized",
      });
    }
  },


  async isAdminOrSuperAdmin(req, res, next) {
    if (!(req.user.role === "admin" || req.user.role === "superadmin")) {
      res.json({
        message: "Anda bukan admin atau superadmin",
      });
      return;
    }
    next();
  },

  async isSuperAdmin(req, res, next) {
    if (!(req.user.role === "superadmin")) {
      res.json({
        message: "Anda bukan superadmin tidak dapat merubah member ke admin",
      });
      return;
    }
    next();
  },

  async create(req, res) {
    try {
      const users = await userService.create(req.body);
      res.status(201).json({
        status: "Data sukses dibuat !!!",
        data: {
          users
        }
      })
    } catch (err) {
      res.status(400).json({
        status: "Failed",
        errors: [err.message]
      })
    }
  },

  async update(req, res) {
    try {
      const users = await userService.update(req.params.id, req.body);
      res.status(200).json({
        status: "Berhasil mengubah member ke admin"
      })
    } catch (err) {
      res.status(400).json({
        status: "Failed",
        errors: [err.message]
      })
    }
  },


  async show(req, res) {
    try {
      const users = await userService.findByPk(req.params.id);
      res.status(200).json({
        status: "OK",
        data: {
          users
        }
      })
    } catch (err) {
      res.status(400).json({
        status: "Failed",
        errors: [err.message]
      })
    }
  },

  async destroy(req, res) {
    try {
      const users = await userService.delete(req.params.id);
      res.status(200).json({
        status: "Data berhasil dihapus",
        data: {
          users
        }
      })
    } catch (err) {
      res.status(400).json({
        status: "Failed",
        errors: [err.message]
      })
    }
  },

  async getUsers(req, res) {
    try {
      const users = await userService.getUsers();
      res.status(200).json({
        status: 'Berhasil',
        data: {
          users
        }
      })
    } catch (err) {
      res.status(400).json({
        status: 'Failed',
        errors: [err.message]
      })
    }
  },
};
