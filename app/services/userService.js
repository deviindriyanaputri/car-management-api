const userRepository = require("../repositories/userRepository");

module.exports = {
  create(requestBody) {
    return userRepository.create(requestBody);
  },

  update(id, requestBody) {
    return userRepository.update(id, requestBody);
  },

  delete(id) {
    return userRepository.delete(id);
  },

  async getUsers() {
    try {
      const users = await userRepository.findAll();

      return {
        data: users,
      };
    } catch (err) {
      throw err;
    }
  },

findByPk(id) {
    return userRepository.findByPk(id)
},

findOne(id) {
    return userRepository.findOne(id)
},

get(id) {
    return userRepository.find(id);
  },
};
