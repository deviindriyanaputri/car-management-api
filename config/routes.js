const express = require("express");
const controllers = require("../app/controllers/");
const res = require("express/lib/response");

const apiRouter = express.Router();

/**
 * TODO: Implement your own API
 *       implementations
 */

// configure and initialization swagger
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

apiRouter.use('/api-docs', swaggerUi.serve);
apiRouter.get('/api-docs', swaggerUi.setup(swaggerDocument));

// User API
apiRouter.get("/api/v1/whoami",controllers.api.v1.userController.authorize,controllers.api.v1.userController.whoAmI
);
apiRouter.post("/api/v1/login", controllers.api.v1.userController.login);
apiRouter.post("/api/v1/register", controllers.api.v1.userController.register);
apiRouter.get("/api/v1/users", controllers.api.v1.userController.getUsers);
apiRouter.get("/api/v1/users/:id", controllers.api.v1.userController.show);

// add admin oleh superadmin saja
apiRouter.put(
  "/api/v1/users/:id",
  controllers.api.v1.userController.authorize,
  controllers.api.v1.userController.isSuperAdmin,
  controllers.api.v1.userController.update
);

//Car API
apiRouter.get("/api/v1/cars", controllers.api.v1.carController.list);

apiRouter.post(
  "/api/v1/cars",
  controllers.api.v1.userController.authorize,
  controllers.api.v1.userController.isAdminOrSuperAdmin,
  controllers.api.v1.carController.create
);

// update car
apiRouter.put(
  "/api/v1/cars/:id",
  controllers.api.v1.userController.authorize,
  controllers.api.v1.userController.isAdminOrSuperAdmin,
  controllers.api.v1.carController.update
);

// read one car
apiRouter.get(
  "/api/v1/cars/:id",
  controllers.api.v1.userController.authorize,
  controllers.api.v1.userController.isAdminOrSuperAdmin,
  controllers.api.v1.carController.show
);

// delete car
apiRouter.delete(
  "/api/v1/cars/:id",
  controllers.api.v1.userController.authorize,
  controllers.api.v1.userController.isAdminOrSuperAdmin,
  controllers.api.v1.carController.destroy
);

/**
 * TODO: Delete this, this is just a demonstration of
 *       error handler
 */
apiRouter.get("/api/v1/errors", () => {
  throw new Error(
    "The Industrial Revolution and its consequences have been a disaster for the human race."
  );
});

apiRouter.use(controllers.api.main.onLost);
apiRouter.use(controllers.api.main.onError);

module.exports = apiRouter;
