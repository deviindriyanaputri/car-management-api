const models = require('../app/models');
module.exports = (modelName) => {
    const Model = models[modelName]

    return async (req, res, next) => {
        try {
            const findUserId = await Model.findByPk(req.params.id)
            if (findUserId.id !== req.user.id) {
                throw new Error("Kamu belum memiliki ijin !!!")
            } else {
                next();
            }
        } catch (err) {
            res.status(400).json({
                status: 'gagal',
                message: err.message
            })
        }
    }
}
