const jwt = require('jsonwebtoken');
const {User} = require('../app/models');

module.exports = async (req, res, next) => {
  try {
    let token = req.headers.authorization;
    let payload = jwt.verify(token, 'rahasia');
    req.user = await User.findByPk(payload.id);
    next();
  } catch {
    res.status(401).json({
      status: "gagal",
      message: 'Token tidak validn'
  })
  }
}
